.. Author: Huynh Van Tai (aka. taiprogramer)

Manage your passwords with "pass"
=================================

Date created: Mar 02 2025


Why do we need that tool?
-------------------------

Nowadays, we use a lot of passwords for multiple things, like bank accounts,
social networks, and applications. Using the same password for all those things
is not a good idea because when one thing gets compromised, we lose all other
things.

The idea is to have a separate password for each thing. It comes with
drawbacks. Although we can choose an easy-to-remember password, as a human, we
can't remember all of them. In reality, we can, but should we? We have other
things to care about, so free your head and use a password manager.

There are many password managers out there, so choosing between them is quite
challenging, especially when it's the first time you try to use them for
managing your password.

I divide password manager into 2 types:

- Cloud-based password manager: YOur passwords will be stored on the cloud
  (There is no real CLOUD in the computer world, it's just someone else's
  computer). It looks like a bad idea because you need to trust the providers
  of the service.

- Offline password manager: Your passwords will be stored on your devices (your
  micro-computer (Laptop), your phone, ...)

The second one looks like the great option, and, it's fucking is :).
For more about security, we need to choose open-source tools, because when more
eyes can look at the source code, the less chance backdoors can appear in them.

What is pass and how to use it?
-------------------------------

pass is a password manager. It allows you to store and use your passwords in a
simple and beautiful manner.

The `official website <https://www.passwordstore.org/>`_ provided an excellent
tutorial about how to use their tool. I wrote this article to give you a
hands-on experience.

Right now, assume that I have 2 Facebook accounts (I don't use Facebook, btw),
2 TikTok accounts (I don't use that app either), 2 X accounts (yaa, you guess
correctly, I also don't have that), the pass will manage my passwords looks
like that

.. code:: text

    ├── Facebook
    │   ├── bar@myaccount.com.gpg
    │   └── foo@myaccount.com.gpg
    ├── TikTok
    │   ├── tik@short.com.gpg
    │   └── tok@short.com.gpg
    └── X
        ├── elone@x.com.gpg
        └── muscle@x.com.gpg

**Prerequisites:**

- You must have a gpg key-pair before using pass. I wrote an article about gpg
  key-pair. You can read it `here <../../2023/Oct/gpg.html>`_.
- Having **pass** tool installed in the system (you can go to the official
  website to download the binary, or use your favorite package managers
  (pacman, apt, apk, ...) to install the binary.

Here are steps to use **pass** for managing your passwords:

- Create a password store, it's just a directory (folder) where all your
  passwords will be stored in an encrypted format. To create a password store,
  type

.. code:: bash

   pass init me@taiprogramer.xyz # replace me@taiprogramer.xyz with your [gpg_id]

**pass** will create a new directory called .password-store, inside that
directory, we have one file .gpg-id (contains your gpg id)

- To save your passwords, type

.. code:: bash

   pass insert Facebook/foo@myaccount.com # replace Facebook/foo@myaccount.com with your corresponding name
   # The output should looks like
   # mkdir: created directory '~/.password-store/Facebook'
   # Enter password for Facebook/foo@myaccount.com:
   # Retype password for Facebook/foo@myaccount.com:

- To list all saved passwords

.. code:: bash

   pass ls
   # Output:
   # Password Store
   # ├── Facebook
   # │   ├── bar@myaccount.com
   # │   └── foo@myaccount.com
   # ├── TikTok
   # │   ├── tik@short.com
   # │   └── tok@short.com
   # └── X
   #     ├── elone@x.com
   #     └── muscle@x.com


- To view or copy a specific password into clipboard

.. code:: bash

   pass show Facebook/foo@myaccount.com
   # 12345678

   pass -c Facebook/foo@myaccount.com
   # Copied Facebook/foo@myaccount.com to clipboard. Will clear in 45 seconds.

====

This document is licensed under the `NON-AI-CC0 <../../../NON-AI-CC0.txt>`_ license.

Examples, recipes, and other code in this document are additionally licensed under the Unlicense.

