.. SPDX-License-Identifier: CC0-1.0
.. Author: Huynh Van Tai (aka. taiprogramer)

==========================
Remove metadata from media
==========================

Date created: Jan 21 2025

What is metadata?
=================

In the simple term, metadata is a data that describes other data. For example,
we have an image data which contains (an actual image), metadata of the image is
what come along with it (GEO Location, ...)

Why we need to remove metadata from media (images)?
===================================================

For privacy purpose, we need to remove metadata from our media, so no one can track you.

exiftool
========

exiftool is a tool that lets you create, edit, remove metadata from the images.

- To view metadata information of any image

.. code:: bash

    exiftool [FILE_NAME]

- To remove all metadata information from an image

.. code:: bash

    exiftool -all= [FILE_NAME] # set all metadata to be none

====

This document is licensed under the `NON-AI-CC0 </NON-AI-CC0.txt>`_ license.

Examples, recipes, and other code in this document are additionally licensed
under the Unlicense.
